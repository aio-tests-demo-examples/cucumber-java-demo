@Login
Feature: Capture the number of trees planted and show relation to global warming

  @P0
  Scenario: Validate user is logged out on changing password
    Given I go to Profile page
    When I change password to a new one
    Then I am logged out


  @P0
  Scenario: Validate sign in page
    Given I am on the home page
    When I select sign in to my account menu
    Then sign in page is opened
    When I enter valid customer email address and password
